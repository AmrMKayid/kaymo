class Config:
    # EMODB_DATA_PATH = './kaymo/data/EmoDB'
    KayMODB_DATA_PATH = './kaymo/data/KayMoDB'
    KayMODB_PREPARED_PATH = './kaymo/data/KayMoDB_Prepared'
    KayMODB_MODELS_PATH = './kaymo/models/pretrained/'
    KayMODB_CLASS_LABELS = ('angry', 'bored', 'calm', 'disgust', 'fearful', 'happy', 'neutral', 'sad', 'surprised')
    EMOTION_DICT = {
        'angry': 0,
        'bored': 1,
        'calm': 2,
        'disgust': 3,
        'fearful': 4,
        'happy': 5,
        'neutral': 6,
        'sad': 7,
        'surprised': 8
    }


    # EMODB_G2E_EMOTIONS = {
    #     'W': 'angry',
    #     'L': 'bored',
    #     'E': 'disgust',
    #     'A': 'fear',
    #     'F': 'happy',
    #     'T': 'sad',
    #     'N': 'neutral',
    # }
    #
    #
    # DATA_PATH = './kaymo/data/CASIA'
    #
    # EMOTION_LABEL = {
    #     'angry': '1',
    #     'fear': '2',
    #     'happy': '3',
    #     'neutral': '4',
    #     'sad': '5',
    #     'surprise': '6'
    # }
    #
    # CLASS_LABELS = ("angry", "fear", "happy", "neutral", "sad", "surprise")
    #
    # MODELS_PATH = './kaymo/models/pretrained/'
