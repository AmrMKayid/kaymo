import numpy as np
import matplotlib.pyplot as plt

from kaymo.configs import Config


def draw_radar(data_prob):
    r"""
    Confidence Probability Radar Chart
    :param data_prob: probability array
    :param class_labels:
    :param num_classes:
    :return:
    """

    plt.clf()  # clear & refresh chart

    angles = np.linspace(0, 2 * np.pi, len(Config.KayMODB_CLASS_LABELS), endpoint=False)  # TODO: NUM_CLASSES FROM Configs
    data = np.concatenate((data_prob, [data_prob[0]]))
    angles = np.concatenate((angles, [angles[0]]))  # TODO: Check this again
    # print('data', data)
    # print('angles', angles)

    fig = plt.figure(1)

    # Polar parameters
    ax = fig.add_subplot(111, polar=True)
    ax.plot(angles, data, 'bo-', linewidth=2)
    ax.fill(angles, data, facecolor='r', alpha=0.25)
    ax.set_thetagrids(angles * 180 / np.pi, Config.KayMODB_CLASS_LABELS)  # TODO: class_labels FROM Configs
    ax.set_title("Emotion Recognition", va='bottom')

    # Where a radar map data maximum
    ax.set_rlim(0, 1)
    ax.grid(True)

    plt.pause(1)  # pause time
