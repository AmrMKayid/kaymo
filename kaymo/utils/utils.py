import os
import sys
from typing import Tuple

import numpy as np
import scipy.io.wavfile as wav
from speechpy.feature import mfcc

# Empirically calculated for the given data set
mean_signal_length = 32000


def get_feature_vector_from_mfcc(file_path: str, flatten: bool,
                                 mfcc_len: int = 39) -> np.ndarray:
    r"""
    Make feature vector from MFCC for the given wav file.

    :param file_path: path to the .wav file that needs to be read.
    :param flatten: Boolean indicating whether to flatten mfcc obtained.
    :param mfcc_len: Number of cepestral co efficients to be consider.
    :return: numpy.ndarray: feature vector of the wav file made from mfcc.
    """

    print(file_path)
    fs, signal = wav.read(file_path)
    s_len = len(signal)
    # pad the signals to have same size if lesser than required
    # else slice them
    if s_len < mean_signal_length:
        pad_len = mean_signal_length - s_len
        pad_rem = pad_len % 2
        pad_len //= 2
        signal = np.pad(signal, (pad_len, pad_len + pad_rem),
                        'constant', constant_values=0)
    else:
        pad_len = s_len - mean_signal_length
        pad_len //= 2
        signal = signal[pad_len:pad_len + mean_signal_length]

    mel_coefficients = mfcc(signal, fs, num_cepstral=mfcc_len)
    if flatten:
        # Flatten the data
        mel_coefficients = np.ravel(mel_coefficients)

    return mel_coefficients


def get_emodb_data(data_path: str, emotions_dict: dict, flatten: bool = True, mfcc_len: int = 39) -> Tuple[
    np.ndarray, np.ndarray]:
    r"""
    Extract EmoDB data for training and testing.
    :param data_path: path to the data set folder
    :param emotions_dict: dict contains emotions and their labels
    :param flatten: Boolean specifying whether to flatten the data or not.
    :param mfcc_len:  Number of mfcc features to take for each frame.
    :return:
    """

    data = []
    labels = []
    cur_dir = os.getcwd()
    sys.stderr.write('Start Reading KayMoDB Data')
    sys.stderr.write('cur_dir: %s\n' % cur_dir)
    os.chdir(data_path)

    wav_file_path = []
    emotion_dirs = os.listdir(data_path)
    # print(emotion_dirs)
    for emotion_dir in emotion_dirs:
        emotion_dir_path = os.path.join(data_path, emotion_dir)
        # print(emotion_dir_path)
        emotion_files = os.listdir(emotion_dir_path)
        # print(emotion_files)

        for file in emotion_files:
            # print(file)
            wav_path = os.path.join(emotion_dir_path, file)
            wav_file_path.append(wav_path)
            # print('emotion_dir:', emotion_dir, 'emotion_label:', emotions_dict[emotion_dir], 'wav file:', wav_path)
    #         print()
            emotion_label = emotions_dict[emotion_dir]
            feature_vector = get_feature_vector_from_mfcc(file_path=wav_path,
                                                          mfcc_len=mfcc_len,
                                                          flatten=flatten)
            data.append(feature_vector)
            labels.append(emotion_label)

        print("Finished reading ({}) emotions".format(emotion_dir))

    sys.stderr.write('\nFinish reading data => EmoDB Data is Ready!\n\n')
    return np.array(data), np.array(labels)
