FACE_SIZE = 48
DATA_PATH = './kaymo/data/fer2013/fer2013.csv'
FACE_CASCADE_PATH = './kaymo/data/haarcascade_frontalface_default.xml'
EMOTIONS = ['angry', 'disgusted', 'fearful', 'happy', 'sad', 'surprised', 'neutral']
