import os
import wave
from datetime import datetime

import matplotlib.pyplot as plt
import numpy as np
from keras.utils import np_utils
from pyaudio import PyAudio, paInt16
from sklearn.model_selection import train_test_split

from kaymo.configs import Config
from kaymo.nets.nn import CNN
from kaymo.utils import utils
from kaymo.utils.viz import draw_radar
from run import get_feature

import librosa
librosa.feature.mfcc()

class Audioer(object):

    def __init__(self):
        self.chunk = 1024  # num_samples
        self.channels = 1
        self.format = paInt16
        self.sampling_rate = 16000
        self.record_seconds = 3
        self.voice_string = []

        self.audio = PyAudio()

    def save_wave(self, filename):
        r"""
        Save the audio file
        :param filename:
        :return:
        """
        wf = wave.open(filename, 'wb')
        wf.setnchannels(self.channels)
        wf.setsampwidth(self.audio.get_sample_size(self.format))
        wf.setframerate(self.sampling_rate)
        # wf.writeframes(b''.join(self.voice_string))
        wf.writeframes(np.array(self.voice_string).tostring())
        wf.close()

    def record_audio(self):
        stream = self.audio.open(
            format=self.format,
            channels=self.channels,
            rate=self.sampling_rate,
            input=True,
            frames_per_buffer=self.chunk)

        print("Start Recording your voice...")

        for i in range(0, int(self.sampling_rate / self.chunk * self.record_seconds)):
            data = stream.read(self.chunk)
            self.voice_string.append(data)

        print("End of Record")
        stream.stop_stream()
        stream.close()
        self.audio.terminate()

        return True


DATA_PATH = os.path.abspath(Config.KayMODB_DATA_PATH)
MODEL_PATH = os.path.abspath(Config.MODELS_PATH)
PREMODEL_PATH = MODEL_PATH + '/Small_CNN0.88_acc_model.h5'

print()


data, labels = (utils.get_emodb_data(os.path.abspath(Config.KayMODB_DATA_PATH), Config.EMOTION_DICT))
print(data.shape, labels.shape)
print(data, labels)


# def extract_data(flatten):
#     data, labels = utils.get_emodb_data(DATA_PATH, emotions_dict=Config.EMODB_EMOTION_DICT,
#                                         flatten=flatten)
#
#     print(labels, labels.shape)
#     x_train, x_test, y_train, y_test = train_test_split(
#         data,
#         labels,
#         test_size=0.2,
#         random_state=42)
#     return np.array(x_train), np.array(x_test), np.array(y_train), np.array(
#         y_test), len(Config.EMODB_EMOTION_DICT)
#
#
# def get_feature_vector(file_path, flatten):
#     return utils.get_feature_vector_from_mfcc(file_path, flatten, mfcc_len=39)
#
#
# def cnn_example():
#     to_flatten = False
#     x_train, x_test, y_train, y_test, num_labels = extract_data(
#         flatten=to_flatten)
#
#     # print('x_train', x_train[0], 'x_train.shape', x_train[0].shape)
#     # print('x_test', x_test[0])
#     # print('y_train', y_train[0:10], 'x_train.shape', y_train[0].shape)
#     # print('y_test', y_test[0])
#     # print(x_train.shape, x_test.shape, y_train.shape, y_test.shape, num_labels)
#
#     y_train = np_utils.to_categorical(y_train)
#     y_test_train = np_utils.to_categorical(y_test)
#     # in_shape = x_train[0].shape
#     x_train = x_train.reshape(x_train.shape + (1,))
#     x_test = x_test.reshape(x_test.shape + (1,))
#
#     print('\n\n\nx_train', x_train[0], 'x_train.shape', x_train[0].shape)
#     print('x_test', x_test[0])
#     print('y_train', y_train[0], 'x_train.shape', y_train[0].shape)
#     print('y_test', y_test[0])
#     print(x_train.shape, x_test.shape, y_train.shape, y_test_train.shape, num_labels)
#
#     model = CNN(input_shape=x_train[0].shape,
#                 num_classes=num_labels, save_path=MODEL_PATH)
#     # model.train(x_train, y_train, x_test, y_test_train, n_epochs=50)
#     # model.evaluate(x_test, y_test)
#
#     print(PREMODEL_PATH)
#     model.trained = True
#     model.load_model(PREMODEL_PATH)
#
#     print('#=============== Testing =================#')
#     abspath = os.path.abspath(__file__)
#     dname = os.path.dirname(abspath)
#     os.chdir(dname)
#     # print(os.getcwd())
#
#     plt.ion()
#
#     while True:
#         rec = Audioer()
#         audio = rec.record_audio()
#         now = datetime.now().strftime("%Y_%m_%d_%H_%M_%S")
#         filename = now + ".wav"
#         rec.save_wave(filename)
#
#         test_file = utils.get_feature_vector_from_mfcc(filename, flatten=to_flatten)
#         test_file_reshaped = test_file.reshape(test_file.shape + (1,))
#
#         emotion_index = model.predict_one(test_file_reshaped)
#         emotion_prob = model.predict_prob(test_file_reshaped)
#         predicted_emotion = Config.EMODB_CLASS_LABELS[emotion_index]
#         print('Predicted Emotion: ', predicted_emotion)
#         print('Emotions Probability: ', emotion_prob[0])
#         draw_radar(emotion_prob[0])
#
#         os.remove(filename)
#
#         # # Extracting acoustic features
#         # data_feature = get_feature(path, 48)
#         # # data_feature = get_feature("test/german10.wav", 48)
#         #
#         # # os.remove(path)
#         #
#         # result = classifier.predict([data_feature])
#         # print(result)
#         # result_prob = classifier.predict_proba([data_feature])[0]
#         # print(Config.CLASS_LABELS)
#         # print('Recognition: ', Config.CLASS_LABELS[int(result)])
#         # print('Probability: ', result_prob)
#         # draw_radar(result_prob)
#
#     # filename = DATA_PATH + '/03/angry/03a01Wa.wav'
#     # test_file = utils.get_feature_vector_from_mfcc(filename, flatten=to_flatten)
#     # test_file_reshaped = test_file.reshape(test_file.shape + (1,))
#     #
#     # emotion_index = model.predict_one(test_file_reshaped)
#     # emotion_prob = model.predict_prob(test_file_reshaped)
#     # predicted_emotion = Config.EMODB_CLASS_LABELS[emotion_index]
#     # print('Predicted Emotion: ', predicted_emotion)
#     # print('Emotions Probability: ', emotion_prob[0])
#     # draw_radar(emotion_prob[0])
#     # print('CNN Done')
#     #
#     # filename = DATA_PATH + '/13/happy/13a01Fd.wav'
#     # test_file = utils.get_feature_vector_from_mfcc(filename, flatten=to_flatten)
#     # test_file_reshaped = test_file.reshape(test_file.shape + (1,))
#     #
#     # emotion_index = model.predict_one(test_file_reshaped)
#     # emotion_prob = model.predict_prob(test_file_reshaped)
#     # predicted_emotion = Config.EMODB_CLASS_LABELS[emotion_index]
#     # print('Predicted Emotion: ', predicted_emotion)
#     # print('Emotions Probability: ', emotion_prob[0])
#     # draw_radar(emotion_prob[0])
#     # print('CNN Done')
#     #
#     # filename = DATA_PATH + '/16/sad/16a01Tb.wav'
#     # test_file = utils.get_feature_vector_from_mfcc(filename, flatten=to_flatten)
#     # test_file_reshaped = test_file.reshape(test_file.shape + (1,))
#     #
#     # emotion_index = model.predict_one(test_file_reshaped)
#     # emotion_prob = model.predict_prob(test_file_reshaped)
#     # predicted_emotion = Config.EMODB_CLASS_LABELS[emotion_index]
#     # print('Predicted Emotion: ', predicted_emotion)
#     # print('Emotions Probability: ', emotion_prob[0])
#     # draw_radar(emotion_prob[0])
#     # print('CNN Done')
#
#
# if __name__ == "__main__":
#     cnn_example()
