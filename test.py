import os
import wave
import joblib
import numpy as np
from datetime import datetime
import matplotlib.pyplot as plt
from pyaudio import PyAudio, paInt16

from run import get_feature
from kaymo.configs import Config
from kaymo.utils.viz import draw_radar


class Audioer(object):

    def __init__(self):
        self.chunk = 1024  # num_samples
        self.channels = 2
        self.format = paInt16
        self.sampling_rate = 8000
        self.record_seconds = 1
        self.voice_string = []

        self.audio = PyAudio()

    def save_wave(self, filename):
        r"""
        Save the audio file
        :param filename:
        :return:
        """
        wf = wave.open(filename, 'wb')
        wf.setnchannels(self.channels)
        wf.setsampwidth(self.audio.get_sample_size(self.format))
        wf.setframerate(self.sampling_rate)
        # wf.writeframes(b''.join(self.voice_string))
        wf.writeframes(np.array(self.voice_string).tostring())
        wf.close()

    def record_audio(self):
        stream = self.audio.open(
            format=self.format,
            channels=self.channels,
            rate=self.sampling_rate,
            input=True,
            frames_per_buffer=self.chunk)

        print("Start Recording your voice...")

        for i in range(0, int(self.sampling_rate / self.chunk * self.record_seconds)):
            data = stream.read(self.chunk)
            self.voice_string.append(data)

        print("End of Record")
        stream.stop_stream()
        stream.close()
        self.audio.terminate()

        return True


if __name__ == '__main__':

    classifier = joblib.load('./kaymo/models/classifier.m')

    if classifier:
        plt.ion()

        while True:
            rec = Audioer()
            audio = rec.record_audio()
            now = datetime.now().strftime("%Y_%m_%d_%H_%M_%S")
            path = now + ".wav"
            rec.save_wave(path)
            # Extracting acoustic features
            data_feature = get_feature(path, 48)
            # data_feature = get_feature("test/german10.wav", 48)

            #os.remove(path)

            result = classifier.predict([data_feature])
            print(result)
            result_prob = classifier.predict_proba([data_feature])[0]
            print(Config.CLASS_LABELS)
            print('Recognition: ', Config.CLASS_LABELS[int(result)])
            print('Probability: ', result_prob)
            draw_radar(result_prob)

    else:
        print("Initialization failed")
