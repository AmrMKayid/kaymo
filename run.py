import os
import time
import joblib
import librosa
import sklearn
import numpy as np
from sklearn import svm
from random import shuffle

from kaymo.configs import Config


def get_feature(wav_file, mfcc_feature_num=16):
    r"""
    Extract mfcc features for each audio file
    :param wav_file:
    :param mfcc_feature_num:
    :return:
    """

    # audio_sr: audio time series;
    # sampling_rate: floating point time series
    audio_sr, sampling_rate = librosa.load(wav_file)

    mfcc_feature = librosa.feature.mfcc(audio_sr, sampling_rate, n_mfcc=16)
    zcr_feature = librosa.feature.zero_crossing_rate(audio_sr)
    energy_feature = librosa.feature.rmse(audio_sr)
    rms_feature = librosa.feature.rmse(audio_sr)

    mfcc_feature = mfcc_feature.T.flatten()[:mfcc_feature_num]
    zcr_feature = zcr_feature.flatten()
    energy_feature = energy_feature.flatten()
    rms_feature = rms_feature.flatten()

    zcr_feature = np.array([np.mean(zcr_feature)])
    energy_feature = np.array([np.mean(energy_feature)])
    rms_feature = np.array([np.mean(rms_feature)])

    data_feature = np.concatenate((mfcc_feature, zcr_feature, energy_feature,
                                   rms_feature))
    return data_feature


def get_data(mfcc_feature_num=16):
    r"""
    finds the characteristics of all voice files in the dataset and the emotional label of the voice
    :param mfcc_feature_num:
    :return: data_features and data_labels
    """

    wav_file_path = []
    person_dirs = os.listdir(Config.DATA_PATH)

    for person in person_dirs:

        if person.endswith('txt'):
            continue

        emotion_dir_path = os.path.join(Config.DATA_PATH, person)
        emotion_dirs = os.listdir(emotion_dir_path)

        for emotion_dir in emotion_dirs:

            if emotion_dir.endswith('.ini'):
                continue

            emotion_file_path = os.path.join(emotion_dir_path, emotion_dir)
            emotion_files = os.listdir(emotion_file_path)

            for file in emotion_files:
                if not file.endswith('wav'):
                    continue

                wav_path = os.path.join(emotion_file_path, file)
                wav_file_path.append(wav_path)

    # Randomly arrange voice files
    shuffle(wav_file_path)
    data_feature = []
    data_labels = []

    # print(Config.EMOTION_LABEL)

    for wav_file in wav_file_path:
        # print(wav_file)
        # print(wav_file.split('/'))
        # print(wav_file.split('/')[-2])
        # print(Config.EMOTION_LABEL[wav_file.split('/')[-2]])
        data_feature.append(get_feature(wav_file, mfcc_feature_num))
        data_labels.append(int(Config.EMOTION_LABEL[wav_file.split('/')[-2]]))

    return np.array(data_feature), np.array(data_labels)


def train():
    r"""
    train speech emotion recognition model using svm forecast
    :return:
    """

    clf = None
    split_num = 200
    best_accuracy = 0
    best_penalty_param_c = 0
    best_mfcc_feature_num = 0

    for C in range(13, 20):
        for mfcc in range(40, 55):

            start = time.time()
            print("Start")
            print("Getting Data")

            data_feature, data_labels = get_data(mfcc)  # Take 72.5 sec

            print("Finish getting data after {} sec".format(time.time() - start))

            train_data = data_feature[:split_num, :]
            train_label = data_labels[:split_num]
            test_data = data_feature[split_num:, :]
            test_label = data_labels[split_num:]

            clf = svm.SVC(
                decision_function_shape='ovo',
                kernel='rbf',
                C=C,
                gamma=0.001,
                probability=True)

            print("C: {} | mfcc: {}".format(C, mfcc))
            # print("Start training")

            clf.fit(train_data, train_label)

            # print("Finish training after {} sec".format(time.time() - start))

            acc_dict = {}
            for test_x, test_y in zip(test_data, test_label):
                pre = clf.predict([test_x])[0]
                if pre in acc_dict.keys():
                    continue
                acc_dict[pre] = test_y

            current_acc = sklearn.metrics.accuracy_score(
                clf.predict(test_data), test_label)

            print('current_acc:', current_acc)

            if current_acc > best_accuracy:
                best_accuracy = current_acc
                best_penalty_param_c = C
                best_mfcc_feature_num = mfcc
                print('best_accuracy', best_accuracy)
                print('best_penalty_param_c', best_penalty_param_c)
                print('best_mfcc_feature_num', best_mfcc_feature_num)
                print("#===============================================#")

            print()

            # Save the model
            joblib.dump(clf,
                        './kaymo/models/pretrained/svm/C_' + str(C) + '_mfccNum_' + str(mfcc) + '.m')

    print('best_accuracy', best_accuracy)
    print('best_penalty_param_c', best_penalty_param_c)
    print('best_mfcc_feature_num', best_mfcc_feature_num)

    # Save the best model
    joblib.dump(clf,
                './kaymo/models/pretrained/svm/best_classifier.m')


if __name__ == '__main__':
    train()
