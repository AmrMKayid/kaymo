from pydub import AudioSegment


t1 = 0 #Works in milliseconds
offset = 12000
t2 = t1 + offset
for i in range(12):
   print(t1, t2)
   newAudio = AudioSegment.from_wav("./german.wav")
   newAudio = newAudio[t1:t2]
   newAudio.export('./german{}.wav'.format(i), format="wav") #Exports to a wav file in the current path.
   t1, t2 = t2, t2 + offset
