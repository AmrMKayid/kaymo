import os
import shutil

wav_data = os.listdir("KayMoDB")


def prepare_emodb():
    EMOTIONS = {
        'W': 'angry',
        'L': 'bored',
        'E': 'disgust',
        'A': 'feardful',
        'F': 'happy',
        'T': 'sad',
        'N': 'neutral',

    }

    wav_file_path = []

    for wav_file in wav_data:
        if not wav_file.endswith('wav'):
            continue
        wav_info, ext = wav_file.split('.')
        # print(wav_info)
        speaker_num, text, emotion, extra = wav_info[:2], wav_info[2:5], wav_info[5], wav_info[6]
        # print(speaker_num, text, emotion, extra)
        emotion_path = "KayMoDB/" + EMOTIONS[emotion]
        os.makedirs(emotion_path, exist_ok=True)

        # emotion_path = path + "/" + EMOTIONS[emotion]
        # os.makedirs(emotion_path, exist_ok=True)

        # print("./EmoDB_Prepared/" + wav_file, emotion_path + '/' + wav_file)
        shutil.move("./KayMoDB/" + wav_file, emotion_path + '/' + wav_file)


def prepare_ravdees():
    EMOTIONS = {
        '01': 'neutral',
        '02': 'calm',  # New
        '03': 'happy',
        '04': 'sad',
        '05': 'angry',
        '06': 'fearful',
        '07': 'disgust',
        '08': 'surprised'  # New
    }

    wav_file_path = []

    for folder in wav_data:
        if not folder.startswith('Actor'):
            continue

        actor = os.listdir("./KayMoDB/" + folder)
        # print('###########################################' , len(actor))
        for wav_file in actor:
            if not wav_file.endswith('wav'):
                continue
            wav_info, ext = wav_file.split('.')
            emotion = wav_info.split('-')[2]
            print(EMOTIONS[emotion])

            # print("./KayMoDB/" + folder + '/' + wav_file)
            emotion_path = "./KayMoDB/" + EMOTIONS[emotion]
            os.makedirs(emotion_path, exist_ok=True)

            shutil.move("./KayMoDB/" + folder + '/' + wav_file, emotion_path + '/' + wav_file)


def prepare_savee():
    EMOTIONS = {
        'a': 'angry',
        'd': 'disgust',
        'f': 'fearful',
        'h': 'happy',
        'n': 'neutral',
        'sa': 'sad',
        'su': 'surprised'
    }

    wav_file_path = []

    for folder in wav_data:
        if not folder.startswith('Actor'):
            continue

        actor = os.listdir("./KayMoDB/" + folder)
        # print('###########################################' , len(actor))
        for wav_file in actor:
            if not wav_file.endswith('wav'):
                continue
            wav_info, ext = wav_file.split('.')
            emotion = wav_info[:2]
            if not emotion.startswith('s'):
                emotion = emotion[0]
            print(EMOTIONS[emotion])

            # print("./KayMoDB/" + folder + '/' + wav_file)
            emotion_path = "./KayMoDB/" + EMOTIONS[emotion]
            os.makedirs(emotion_path, exist_ok=True)

            shutil.move("./KayMoDB/" + folder + '/' + wav_file, emotion_path + '/' + wav_file)

# prepare_emodb()
# prepare_ravdees()
# prepare_savee()